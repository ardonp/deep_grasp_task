# Deep Grasp Task
<<<<<<< HEAD
<img src="https://picknik.ai/assets/images/logo.jpg" width="120">

## Overview
This package constructs a pick and place task using the MoveIt Task Constructor. It also provides tools for collecting depth sensor data that can be used with the deep grasping libraries.
=======
This package is adapted from MoveIt Task Constructor. At this stage it allows to reproduce a pick-and-place task. It provides tools for collecting depth sensor data that can be used with the deep grasping libraries.

## Installation
```
git clone git clone https://ardonpaola@bitbucket.org/ardonp/deep_grasp_task.git
catkin build
```
>>>>>>> 2b68647adb8656fe71f6122b7cf422cea5c8f000

## Nodes
### deep_grasp_demo
This node is renamed at launch to mtc_tutorial. It constructs the pick and place task and adds objects to the planning scene, published on the `planning_scene` topic.

## Config
* camera.intr: depth camera intrinsic parameters used by Dex-Net

* camera.yaml: depth camera extrinsic parameters used to transform the grasp candidates from the depth camera optical link to the frame of the base link of the robot

* panda_object.yaml: Panda configurations and object pick and place configurations
<<<<<<< HEAD
=======


## Depth Sensor Data
### Collecting Data using Gazebo
Perhaps you want to collect depth sensor data on an object and use fake controllers to execute the motion plan. The launch file `sensor_data_gazebo.launch` will launch a `process_image_server` and a `point_cloud_server` node. These will provide services to save either images or point clouds.
Images will be saved to `moveit_task_constructor_dexnet/data/images`.

To collect either images or point clouds run:
```
roslaunch deep_grasp_task sensor_data_gazebo.launch
```

To save the depth and color images:
```
rosservice call /save_images "depth_file: 'my_depth_image.png'
color_file: 'my_color_image.png'"

```

To save a point cloud:
```
rosservice call /save_point_cloud "cloud_file: 'my_cloud_file.pcd'"
```


## Camera View Point
Initially, the camera is setup to view the cylinder from the side of the robot. It is useful particularly for Dex-Net to place the camera in an overhead position above the object. To change the camera view point there are a few files to modify. You can move the camera to a preset overhead position or follow the general format to create a new position.

First, modify the camera or the panda + camera urdf.

If you want to move the camera position just for collecting sensor data, in `deep_grasp_task/urdf/camera/camera.urdf.xacro` change the camera xacro macro line to read:
```XML
<xacro:kinect_camera parent_link="world" cam_px="0.5" cam_pz="0.7" cam_op="1.57079632679"/>
```

If you want to move the camera position and use the robot to execute trajectories. Go to `deep_grasp_task/urdf/robots/panda_camera.urdf.xacro` and change the camera xacro macro line to read:
```XML
<xacro:kinect_camera parent_link="panda_link0" cam_px="0.5" cam_pz="0.7" cam_op="1.57079632679"/>
```

Next, specify the transformation from the robot base link to the camera link.

Change `deep_grasp_task/config/calib/camera.yaml` to read:
```YAML
trans_base_cam: [0.500, 0.000, 0.700, 0.707, 0.000, 0.707, 0.000]
```

Finally, this is optional depending on whether the camera is added to the planning scene. If the camera is in the planning scene you need to modify `deep_grasp_task/config/panda_object.yaml` to read:
```YAML
spawn_camera: true
camera_pose: [0.5, 0, 0.7, 0, 1.571, 1.571]
```

## Known Issues
1) When running with Gazebo
```
ros.moveit_simple_controller_manager.SimpleControllerManager: Controller panda_hand_controller failed with error GOAL_TOLERANCE_VIOLATED:
ros.moveit_ros_planning.trajectory_execution_manager: Controller handle panda_hand_controller reports status ABORTED
```

2) Planning may fail

If using GPD, increase the number of points sampled by setting `num_samples` in `config/gpd_config.yaml`.
Another option is to run either algorithm again. Maybe low quality grasps were sampled or they were not kinematically feasible.
>>>>>>> 2b68647adb8656fe71f6122b7cf422cea5c8f000
